/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Plugins
import { registerPlugins } from "@/plugins";

// Components
import App from "./App.vue";

// Composables
import { createApp } from "vue";

// Firebase
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBqSNTV7zI7NlNvaMbQqb2u4Z_cxWQ0AOk",
  authDomain: "palpedia-c7ba8.firebaseapp.com",
  databaseURL: "https://palpedia-c7ba8-default-rtdb.firebaseio.com",
  projectId: "palpedia-c7ba8",
  storageBucket: "palpedia-c7ba8.appspot.com",
  messagingSenderId: "221543221780",
  appId: "1:221543221780:web:9581cb1eeeeedbc0493501",
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const db = getFirestore(firebase);

const app = createApp(App);

registerPlugins(app);

app.use(firebase).mount("#app");

export { db };
